# -*- coding: utf-8 -*-



## рабочие функции
def addWord(line, dict, deep):
    # выборка из deep-букв слова
    for i in range (0, len(line)-deep+1):
        key = line[i: i+deep]
        #print (key, i)
        if key not in dict:
            dict[key] = 1
        else:
            dict[key] += 1
    return 0

def getWordCount(line, dict):
    return  dict.get(line)
    
    

    
## константы I/O
PATH_TRAIN = 'train.csv'
PATH_TEST = 'test.csv'
PATH_PRED = 'pred.csv'





## обработка
print('Reading started . . .')

dict = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}]  #25 + 1
libr = {}
max_deep = 24
hard_deep = 4
for deep in range(1, max_deep+1):
    print ('\tProcessing at deep =', deep, 'of', max_deep)
    fl = open(PATH_TRAIN, 'rt', encoding='utf-8')
    fl.readline()
    for line in fl:
        Id, Sample, Prediction = line.strip().split(',')
        word1, word2 = Prediction.split(' ')
        libr[word2[:hard_deep]] = word2

        #print (word1, word2)
        addWord(word1 + ' ' + word2 + ' ', dict[deep], deep)
    fl.close()

print ('Reading done\n')
#for tmp in dict3.keys():
#   print ('dict[', tmp, ']', dict3[tmp])
#print (u'удн', getWordCount(u'удн', dict3))





## выполнение предсказаний
print ('Writing started . . .')
fl = open(PATH_TEST, 'rt', encoding='utf-8')
fl.readline()
out_fl = open(PATH_PRED, 'wt', encoding='utf-8')
out_fl.write('Id,Prediction\n')
alph = [u' ',u'а',u'б',u'в',u'г',u'д',u'е',u'ё',u'ж',u'з',u'и',u'й',u'к',u'л',u'м',u'н',u'о',u'п',u'р',u'с',u'т',u'у',u'ф',u'х',u'ц',u'ч',u'ш',u'щ',u'ъ',u'ы',u'ь',u'э',u'ю',u'я']

count = 0

for line in fl:
    count += 1
    if (count >= 50000):
        count = 0
        print ('\t another 50k words are processed')

    Id, Sample = line.strip().split(',')
    word1, word2_chunk = Sample.split(' ')
    answ = word1 + ' ' + word2_chunk
    
    decrement = 0
    while (True):    
        while (True):
            chr = u' '
            deep = len(answ)
            if (deep >= max_deep):
                deep = max_deep-1
            key = answ[len(answ)-deep:]
            max_c = 0
            allNone = True
            #print ('\t', answ, deep, key)
            for tmp in alph:
                tmp_c = getWordCount(key + tmp, dict[deep+1])
                #print ('\t', tmp, tmp_c)
                if tmp_c != None:
                    #print (tmp)
                    if tmp_c > max_c:
                        max_c = tmp_c
                        chr = tmp
                        allNone = False
            if (chr == u' '):
                #print (word2_chunk, answ)
                break
            answ = answ + chr
            
        if (allNone == False):
            #out_fl.write('%s,%s %s\n' % (Id, word1, answ) )
            out_fl.write('%s,%s\n' % (Id, answ) )
            print ('nonoutism')
            break
        else:
            decrement += 1
            if (decrement > len(word2_chunk) + 2):
                out_fl.write('%s,%s %s\n' % (Id, word1, word2_chunk + u'ь') )
                #print ('outism')
                break
        
        #if (allNone) && (decrement > len(word2_chunk) + 2):
        decrement = 0
        while (True):
            allNone = True
            Id, Sample = line.strip().split(',')
            word1, word2_chunk = Sample.split(' ')
            answ = word2_chunk    
            while (True):
                chr = u' '
                deep = len(answ)
                if (deep >= max_deep):
                    deep = max_deep-1
                deep -= decrement
                key = answ[len(answ)-deep:]
                max_c = 0                
                for tmp in alph:
                    tmp_c = getWordCount(key + tmp, dict1[deep+1])                    
                    if tmp_c != None:
                        if tmp_c > max_c:
                            max_c = tmp_c
                            chr = tmp
                            allNone = False
                #f (allNone == True):
                #    print ('allNone', word2_chunk)
                if (chr == u' '):
                    break
                answ = answ + chr    
            if (allNone == False):
                out_fl.write('%s,%s %s\n' % (Id, word1, answ) )
                break
            else:
                decrement += 1
                if (decrement > len(word2_chunk) + 2):
                    out_fl.write('%s,%s %s\n' % (Id, word1, word2_chunk + u'ь') )
                    print ('outism')
                    break
            
fl.close()
out_fl.close()
print ('Writing done\n')