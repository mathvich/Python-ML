import numpy as np
import matplotlib.pyplot as plt
import math
import matplotlib.image as mpimg

image = mpimg.imread('./mailru.jpg')
plt.axis("off")
plt.imshow(image)
plt.show()
data = image.reshape((image.shape[0]*image.shape[1],3))
print(type(data))
print('len =', len(data))
plt.imshow(data)
get_ipython().run_line_magic('matplotlib', 'inline')


# # Задание:
# 
# Реализовать алгоритм кластеризации k-means

class KMeans:
    def __init__(self, n_clusters=2, metric='euclidean', max_iter=300):
        '''
        n_clusters - число кластеров
        metric - метрика
        max_iter - максимальное число итераций
        '''
        self.n_clusters = n_clusters
        self.metric = metric
        self.max_iter = max_iter
        self.centers = np.array([])

    @staticmethod
    def distance(vector1, vector2):
        '''
        Определяем функцию расстояния
        '''
        a = np.array(vector1)
        b = np.array(vector2)
        sqDst = sum((a-b)**2);
        
        return math.sqrt(sqDst)
        
    def predict(self, X):
        '''
        Предсказываем попадание объектов из X в конкретный кластер
        '''
        answ = np.zeros(len(X),dtype=np.intc)
        dist = np.zeros((len(X), self.n_clusters))
        for i in range(len(X)):
            for j in range(self.n_clusters):
                dist[i,j]=self.distance(X[i], self.centers[j])
        for i in range(len(dist)):
            answ[i] = np.argmin(dist[i])
        #print('len =', len(answ))
        return answ

    def fit(self, X):
        '''
        Шаг 1 - Инизиализируем начальные положения центров кластеров
        '''
        self.centers = np.zeros((self.n_clusters, len(X[0])))
        for i in range(self.n_clusters):
            self.centers[i] = X[i]
        
        '''
        Шаг 2 - Выполняем уточнение положения центров кластеров до тех пор, пока 
        не будет превышено значение max_iter или центры кластеров не будут меняться 
        '''
        for step in range(self.max_iter):            
            '''
            Шаг 2.1 - Вычисляем расстояние до цетров кластеров
            '''
            '''
            Шаг 2.2 - Для каждого объекта находим argmin от расстояний до центров
            '''
            tmp = self.predict(X)
            
            '''
            Шаг 2.3 - Уточняеням положения центров кластеров
            '''
            new_centers = 0.0*self.centers
            counts = np.zeros(self.n_clusters)
            for i in range(len(X)):
                #print('i =', i, 'tmp[i] =', tmp[i])
                new_centers[tmp[i]] += X[i]
                counts[tmp[i]] += 1
            for i in range(self.n_clusters):
                new_centers[i] /= (1.0+counts[i])
            self.centers = np.copy(new_centers)
            
        '''
        Шаг 3 - Сохраняем положения центров кластеров
        ''' 
        #self.centers = NewClusters #centers
        #self.clusters = NewClusters
        
        '''
        Шаг 4 - Возвращяем предсказание
        '''        
        return self.predict(X)

km = KMeans()
newData = km.fit(data)

